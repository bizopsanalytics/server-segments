with land_year as
        (--identify the year a customer first landed as a customer
                select  email_domain,
                        base_product, 
                        financial_year, 
                        platform,
                        max(user_limit) as user_limit,
                        min(date) as min_date
                from    public.sale
                where   sale_type = 'New to New'
                and     base_product in ('JIRA', 'JIRA Core', 'JIRA Software','JIRA Service Desk')
                and     platform = 'Server' 
                group by 1,2,3,4
        )
        
        select a.financial_year,
               avg(a.user_limit) as avg_user,
               count(distinct a.email_domain) as email_count, 
               sum(b.amount)/count(distinct a.email_domain) as asp
        from land_year as a
        left join public.sale as b on a.email_domain = b.email_domain and a.base_product = b.base_product
        where b.date <= a.min_date + interval '365' DAY
        and a.user_limit not in (987654321)
        group by 1
        order by 1 asc
        
     
        