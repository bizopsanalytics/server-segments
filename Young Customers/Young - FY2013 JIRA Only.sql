--young customer segment 180 day renewal rate

with land_year as
        (--identify the year a customer first landed as a customer
        select  email_domain, 
                financial_year, 
                platform,
                min(date) as min_date
        from    public.sale
        where   sale_type = 'New to New'
        group by 1,2,3
        ),
sen_date as
        (--find the land date of each sen
        select  sen, 
                email_domain,
                user_limit,
                region,
                min(date) as min_date
        from    public.sale
        where   cast(date as date) <= cast('2013-06-30' as date)
        and     base_product in ('JIRA','JIRA Software','JIRA Core','JIRA Service Desk')
        and     license_level = 'Full'
        group by 1,2,3,4
        ),
sen_base as 
        (--find the sens that were landed in a period by a customer landed in a particular year
        select  a.email_domain,
                b.sen 
        from    land_year as a
        left join public.sale as b on a.email_domain = b.email_domain
        left join sen_date as c on b.sen = c.sen
        where   a.financial_year = 'FY2013'
        and     b.financial_year = 'FY2013'
        and     b.license_level = 'Full'
        and     a.platform = 'Server'
        and     b.base_product in ('JIRA','JIRA Software','JIRA Core','JIRA Service Desk')
        and     cast(b.date as date) <= cast('2013-06-30' as date)
        and     cast(a.min_date as date) <= cast('2013-06-30' as date)
        group by 1,2
                    ),
renew_check as 
        (--identify the number of SENs created in the period and how many renewed within 180 days of the sen's land date.
        select
        a.sen,
        --b.base_product,
        --c.region,
        --c.user_limit,
        --d.company_size,
        count(distinct a.email_domain) as email_count, 
        count(distinct a.sen) as sen_count, 
        count(distinct case when b.sale_type = 'Renewal' and b.date < c.min_date + 545 then a.sen end) as "renew",
        count(distinct case when b.sale_type = 'Upgrade' and b.date < c.min_date + 545 then a.sen end) as "upgrade"
        --a.sen
        from sen_base as a
        left join public.sale as b on a.sen = b.sen
        left join sen_date as c on a.sen = c.sen
        --left join playground_bizops.customer_size as d on a.email_domain = d.email_domain
        group by 1
        --order by 1,2,3
        )
        select  sum(a.sen_count) as sen_count,
                count(distinct case when a.renew = 1 and a.upgrade = 0 then a.sen end) as "renew",
                count(distinct case when a.renew = 0 and a.upgrade = 1 then a.sen end) as "upgrade only",
                count(distinct case when a.renew = 1 and a.upgrade = 1 then a.sen end) as "renew and upgrade"
        from renew_check as a;


