
--first year contribution by platform
with land_year as
(
        select email_domain, financial_year, platform
        from public.sale
        where sale_type = 'New to New'
        group by 1,2,3
)
select  count(distinct b.sen     
from land_year as a
left join public.sale as b on a.email_domain = b.email_domain
where a.financial_year = 'FY2015'
and b.financial_year = 'FY2015'
and b.license_level = 'Full'
and b.platform = 'Server'
group by 1
order by 1