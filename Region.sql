--base for heatmap
with land_user_tier as ( 
select  sen,
        min(date) as min_date
from public.sale    
where   format_datetime(date,'%Y%m%d') < '20150331'  
and     license_level = 'Full'
and     platform = 'Server'
--and sold_via_partner = false
--and region = 'APAC'
group by 1
),
renewed_sens as (
select  a.sen
from    land_user_tier as a
left join public.sale as b on a.sen = b.sen
where   b.sale_type = 'Renewal'
and     b.date > a.min_date
and     b.platform = 'Server'
group by 1
)
select  count(distinct a.sen) as total_sen,
        count(distinct b.sen) as total_renew
from    land_user_tier as a
left join renewed_sens as b on a.sen = b.sen