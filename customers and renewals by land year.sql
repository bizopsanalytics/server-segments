with land_year as
(
        select email_domain, financial_year, platform
        from public.sale
        where sale_type = 'New to New'
        group by 1,2,3
),
sen_date as
(
        select sen, 
        email_domain,
        min(date)
        from public.sale
        where license_level = 'Full'
        group by 1,2
)
select  b.financial_year, 
        count(distinct b.email_domain) as cust_count, 
        count(distinct a.sen) as sen_count,
        count(distinct case when c.sale_type = 'Renewal' then a.sen end) as "renewed"
from sen_date as a
left join land_year as b on a.email_domain = b.email_domain
left join sale as c on a.sen = c.sen
group by 1
