select  
        
        product,
        stage_reason,
        case
        when lost_date between '2010-07-01' and '2011-06-30' then 'FY2010'
        when lost_date between '2011-07-01' and '2012-06-30' then 'FY2011'
        when lost_date between '2012-07-01' and '2013-06-30' then 'FY2012'
        when lost_date between '2013-07-01' and '2014-06-30' then 'FY2013' 
        when lost_date between '2014-07-01' and '2015-06-30' then 'FY2014'
        when lost_date between '2015-07-01' and '2016-06-30' then 'FY2015'
        end as lost_year,
        count(distinct sen) as sen_count
from playground_intsys.renewals_lost 
where years_owned = 1
group by 1,2,3
order by 1,2,3