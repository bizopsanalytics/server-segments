
--first year contribution by platform
with land_year as
(
        select email_domain, financial_year, platform
        from public.sale
        where sale_type = 'New to New'
        group by 1,2,3
),
sen_date as
(select sen, 
        email_domain,
        min(date) as min_date
from public.sale
where cast(date as date) <= cast('2014-09-30' as date)
group by 1,2
),
sen_base as (
select  
        a.email_domain,
        b.sen 
from land_year as a
left join public.sale as b on a.email_domain = b.email_domain
left join sen_date as c on b.sen = c.sen
where a.financial_year = 'FY2015'
and b.financial_year = 'FY2015'
and b.license_level = 'Full'
and a.platform = 'Server'
and cast(b.date as date) <= cast('2014-09-30' as date)
group by 1,2
)
select count(distinct a.sen)
from sen_base as a
left join public.sale as b on a.sen = b.sen
left join sen_date as c on a.sen = c.sen
where b.sale_type = 'Renewal'
and b.date < c.min_date + interval '545' DAY 