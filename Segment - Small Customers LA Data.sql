WITH churn_buckets AS
(
SELECT *, 
CASE 
WHEN stage_reason IN(
'Company Out of Business', 
'Consolidating Instances',
'Cross-graded to JIRA Core', 
'Downgrade to Starter',
'Using another instance (same product)',
'Moved to Cloud',
'JIRA Renaissance Package / Pricing',
'Moved to another BTF product',
'Moved to Data Center' ) THEN 'Non-regretted'
WHEN stage_reason IN(
'Don''t want to renew', 
'No longer use Product', 
'No need for Support / Updates',
'Too expensive / No budget',
'Using Competitor''s Product',
'Waiting to Update Product',
'Not satisfied with Support / Updates',
'Waiting to Upgrade User-Tiers',
'Outstanding support issues / feature requests',
'Not in Production' ) THEN 'Regretted'
WHEN stage_reason IN(
'Contact info unavailable' ,
'Invalid Contact Information',
'Invalid Opportunity',
'No Response',
'Reason not given',
'Unable to Contact', 
'Project Completed',
'Other/no additional information given',
'Company Out of Business',
'Other (see General Comments)' ) OR stage_reason is null THEN 'Exclude'
END AS churn_type
FROM playground_intsys.renewals_lost
WHERE lost_date >= '2015-07-01'
),
b AS (
SELECT sen,
CASE 
WHEN amount BETWEEN 0 and 499 THEN 'Less than $500'
WHEN amount BETWEEN 500 and 999 THEN 'Between $500 and $999'
WHEN amount BETWEEN 1000 and 4999 THEN 'Between $1k and $5k'
WHEN amount BETWEEN 5000 and 9999 THEN 'Between $5k and $10k'
WHEN amount >= 10000 THEN 'Greater than $10k' END AS value_bucket,
stage_reason,
description
FROM churn_buckets
GROUP BY 1,2,3,4
ORDER BY 2)
SELECT *
FROM b
where sen in (

        with land_year as
        (
        select  a.email_domain, 
                max(b.company_size) as company_size,
                min(a.date) as min_date,
                min(a.financial_year) as land_FY
        from    public.sale as a
        left join playground_bizops.customer_size as b on a.email_domain = b.email_domain
        -- left join zone_bizops.customer_size as b on a.email_domain = b.email_domain --socrates version
        where   sale_type = 'New to New'
        and     b.company_size <=200
        and     a.platform = 'Server'
        and     a.financial_year <> 'FY2017'
        group by 1
        )
        select  b.sen       
        from    land_year as a
        left join public.sale as b on a.email_domain = b.email_domain
        where   a.land_fy <= b.financial_year
        and     b.financial_year <> 'FY2017'
        group by 1
        order by 1
        )
