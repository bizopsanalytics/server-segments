with young_cust as
        ( --identify all customers who first came to Server in FY2015.
        select  distinct email_domain,
                region,
                min(date) as land_date
        from    public.sale
        where   sale_type = 'New to New'
        and     platform = 'Server'
        and     financial_year in ('FY2013','FY2014','FY2015')
        group by 1,2
        ),
sen_start as 
        (--identify all the SENs that customers who landed in FY2015 purchased since landing
        select  a.sen,
                a.base_product, 
                a.email_domain,
                min(a.date) as min_date
        from    public.sale as a    
        left join young_cust as b on a.email_domain = b.email_domain  
        where   a.email_domain in (select email_domain from young_cust)
        and     a.date >= b.land_date
        group by 1,2,3
        ),
la_data as 
        ( --generate Loyalty Advocate data
        WITH churn_buckets AS
        (
        SELECT *, 
        CASE 
        WHEN stage_reason IN(
        'Company Out of Business', 
        'Consolidating Instances',
        'Cross-graded to JIRA Core', 
        'Downgrade to Starter',
        'Using another instance (same product)',
        'Moved to Cloud',
        'JIRA Renaissance Package / Pricing',
        'Moved to another BTF product',
        'Moved to Data Center' ) THEN 'Non-regretted'
        WHEN stage_reason IN(
        'Don''t want to renew', 
        'No longer use Product', 
        'No need for Support / Updates',
        'Too expensive / No budget',
        'Using Competitor''s Product',
        'Waiting to Update Product',
        'Not satisfied with Support / Updates',
        'Waiting to Upgrade User-Tiers',
        'Outstanding support issues / feature requests',
        'Not in Production' ) THEN 'Regretted'
        WHEN stage_reason IN(
        'Contact info unavailable' ,
        'Invalid Contact Information',
        'Invalid Opportunity',
        'No Response',
        'Reason not given',
        'Unable to Contact', 
        'Project Completed',
        'Other/no additional information given',
        'Company Out of Business',
        'Other (see General Comments)' ) OR stage_reason is null THEN 'Exclude'
        END AS churn_type
        FROM playground_intsys.renewals_lost
        WHERE lost_date >= '2015-07-01'
        ),
        b AS (
        SELECT sen,
        CASE 
        WHEN amount BETWEEN 0 and 499 THEN 'Less than $500'
        WHEN amount BETWEEN 500 and 999 THEN 'Between $500 and $999'
        WHEN amount BETWEEN 1000 and 4999 THEN 'Between $1k and $5k'
        WHEN amount BETWEEN 5000 and 9999 THEN 'Between $5k and $10k'
        WHEN amount >= 10000 THEN 'Greater than $10k' END AS value_bucket,
        stage_reason, 
        description
        FROM    churn_buckets
        GROUP BY 1,2,3,4
        ORDER BY 2
        )
        SELECT distinct sen, value_bucket, stage_reason, description
        FROM b 
        )        
        select          a.email_domain,
                        a.region, 
                        b.base_product,
                        b.sen, 
                        c.stage_reason
                        -- c.description           
        from            young_cust as a
        left join       sen_start  as b on a.email_domain = b.email_domain
        left join       la_data    as c on b.sen = c.sen
        where           c.stage_reason is not null

       
