WITH churn_buckets AS
(
SELECT *, 
CASE 
WHEN stage_reason IN(
'Company Out of Business', 
'Consolidating Instances',
'Cross-graded to JIRA Core', 
'Downgrade to Starter',
'Using another instance (same product)',
'Moved to Cloud',
'JIRA Renaissance Package / Pricing',
'Moved to another BTF product',
'Moved to Data Center' ) THEN 'Non-regretted'
WHEN stage_reason IN(
'Don''t want to renew', 
'No longer use Product', 
'No need for Support / Updates',
'Too expensive / No budget',
'Using Competitor''s Product',
'Waiting to Update Product',
'Not satisfied with Support / Updates',
'Waiting to Upgrade User-Tiers',
'Outstanding support issues / feature requests',
'Not in Production' ) THEN 'Regretted'
WHEN stage_reason IN(
'Contact info unavailable' ,
'Invalid Contact Information',
'Invalid Opportunity',
'No Response',
'Reason not given',
'Unable to Contact', 
'Project Completed',
'Other/no additional information given',
'Company Out of Business',
'Other (see General Comments)' ) OR stage_reason is null THEN 'Exclude'
END AS churn_type
FROM playground_intsys.renewals_lost
WHERE lost_date >= '2015-07-01'
),
b AS (
SELECT sen,
CASE 
WHEN amount BETWEEN 0 and 499 THEN 'Less than $500'
WHEN amount BETWEEN 500 and 999 THEN 'Between $500 and $999'
WHEN amount BETWEEN 1000 and 4999 THEN 'Between $1k and $5k'
WHEN amount BETWEEN 5000 and 9999 THEN 'Between $5k and $10k'
WHEN amount >= 10000 THEN 'Greater than $10k' END AS value_bucket,
stage_reason
FROM churn_buckets
GROUP BY 1,2,3
ORDER BY 2)
SELECT stage_reason, count(sen)
FROM b
where sen in (
--all product Server migrations to Cloud
with migrations as (
select
        l.base_product,
        date_month(existing_end_date) as "Month",
        l.license_level,
        t.sen as server_sen
from license l
inner join
(
 select
        lr.tech_email,
        lr.tech_email_domain,
        lr.existing_user_limit,
        existing_end_date, l.sen, 
        lr.tech_region,
        lr.base_product
 from license_renewal lr
 inner join license l on (lr.sen = l.sen::text)
 where  1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and existing_end_date between date_trunc('month',current_date) - interval '61 months' and date_trunc('month',current_date) - interval '1 day'--'1 month 1 day'
        and lr.platform='Server'
        and l.license_level in ('Full')
        and not renewed
) t
on (
        l.tech_email_domain = t.tech_email_domain
        and l.tech_region = t.tech_region
        and l.base_product = t.base_product
        and l.purchase_date between existing_end_date - 90 and existing_end_date + 90
        and l.user_limit >= t.existing_user_limit)
  
where   1 = 1
        --and l.base_product not in ('JIRA', 'JIRA Software')
        and l.platform = 'Data Center' --'Cloud'
        and l.license_level in ('Full')
        and l.purchase_date > date_trunc('month',current_date) - interval '64 months'
--group by 1,2
--order by 1,2
),
list as (
select  a.server_sen,
        min(b.financial_year) as land_year
from migrations as a
left join sale as b on cast(a.server_sen as text) = b.sen
group by 1
)
select cast(server_sen as text)
from list
)
group by 1