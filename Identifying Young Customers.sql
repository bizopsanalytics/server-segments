--first year contribution by platform
with land_year as
(
        select  email_domain, 
                financial_year
        from    public.sale
        where   sale_type = 'New to New'
        and     platform = 'Server'
        group by 1,2,3
)
select  a.financial_year, 
        count(distinct a.email_domain) as new_cust,
        count(distinct b.sen) as sen_count        
from land_year as a
left join public.sale as b on a.email_domain = b.email_domain
where a.financial_year = 'FY2015'
and b.financial_year = 'FY2015'
and b.license_level = 'Full'
group by 1,2
order by 1,2