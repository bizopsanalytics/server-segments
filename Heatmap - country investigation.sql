with land_user_tier as ( 
select  a.sen,
        a.email_domain,
        a.user_limit,
        a.platform,
        a.product_family,
        a.region,
        a.country,
        a.sold_via_partner,
        b.company_size,
        min(a.date) as min_date
from public.sale as a
left join zone_bizops.customer_size as b on a.email_domain = b.email_domain   
where   format_datetime(a.date,'%Y%m%d') < '20150331'  
and     a.license_level = 'Full'
and     a.platform = 'Server'
and     a.base_product in ('JIRA','JIRA Core','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
group by 1,2,3,4,5,6,7,8,9
),
first_date as 
( select a.email_domain, a.sen, min(b.date) as min_date
  from land_user_tier as a
  left join public.sale as b on a.email_domain = b.email_domain
  where sale_type = 'New to New'
  and b.base_product in ('JIRA','JIRA Core', 'JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
  group by 1,2
),
land_prod as
(       select a.email_domain, count(distinct b.base_product) as prod_count
        from first_date as a
        left join public.sale as b on a.email_domain = b.email_domain
        where sale_type = 'New to New'
        and b.base_product in ('JIRA','JIRA Core', 'JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and license_level in ('Full','Starter')
        and a.min_date = b.date
        group by 1
),
cust_age as
( select a.email_domain, financial_year
        from first_date as a
        left join public.sale as b on a.email_domain = b.email_domain
        where sale_type = 'New to New'
        and b.base_product in ('JIRA','JIRA Core', 'JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and license_level in ('Full','Starter')
        and a.min_date = b.date
        group by 1,2
),
renewed_sens as (
select  a.sen
from    land_user_tier as a
left join public.sale as b on a.sen = b.sen
where   b.sale_type = 'Renewal'
and     b.date > a.min_date
and     b.platform = 'Server'
group by 1
),
upgraded_no_renew as (
        select  a.sen
        from    land_user_tier as a
        left join public.sale as b on a.sen = b.sen
        where   b.sale_type = 'Upgrade'
        and     b.date > a.min_date
        and     b.platform = 'Server'
        and     a.sen not in (select sen from renewed_sens)
        ),
combined_renewal as 
        (-- combine the results of renewed sens and upgraded-only sens
        select sen
        from renewed_sens
        
        union
        
        select sen
        from upgraded_no_renew
        ),
addons as (
        select  a.email_domain, 
                a.sen,
                c.base_product,
                count(b.product) as addon_count
                from land_user_tier as a 
                left join public.license as b on a.email_domain = b.tech_email_domain
                left join public.sale as c on a.email_domain = c.email_domain
                where b.platform = 'Server'
                and cast(b.first_sale_date as timestamp) < a.min_date + interval '365' DAY
                and b.base_product = 'Marketplace Addon'
                and a.product_family = b.product_family
                and c.date = a.min_date
        group by 1,2,3
),
no_addons as
(
select sen 
from land_user_tier
where sen not in (
                select  a.sen
                from land_user_tier as a 
                left join public.license as b on a.email_domain = b.tech_email_domain
                where b.platform = 'Server'
                and cast(b.first_sale_date as timestamp) < a.min_date + interval '365' DAY
                and b.base_product = 'Marketplace Addon'
                and a.product_family = b.product_family
                group by 1)
),
cross_sell as (
       select  a.sen
                from land_user_tier as a 
                left join public.sale as c on a.email_domain = c.email_domain
                where cast(c.date as timestamp) < a.min_date + interval '365' DAY
                and a.product_family <> c.product_family
                and c.sale_type = 'New to Existing'
                and c.date <> a.min_date
                and c.base_product in('JIRA','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        group by 1
)       
select  a.region,
        a.country,
        --a.company_size,
        --a.user_limit,
        count(a.sen) as total_sen,
        count(b.sen) as total_renew,
        count(case when sold_via_partner = true then a.sen else null end) as "Expert_SEN",
        count(case when sold_via_partner = true then b.sen else null end) as "Expert_Renew",
        count(case when sold_via_partner = false then a.sen else null end) as "Direct_SEN",
        count(case when sold_via_partner = false then b.sen else null end) as "Direct_Renew"
           
from    land_user_tier as a
left join combined_renewal as b on a.sen = b.sen
left join land_prod as c on a.email_domain = c.email_domain     --linked to product lands
left join cust_age as d on a.email_domain = d.email_domain
where region <> 'Unknown'
--and a.sen in (select sen from addons where addon_count >= 1)    -- this shows only addon group when active
--and a.sen in (select * from no_addons)                        -- this shows only no-addon group when active
--and a.sen in (select * from cross_sell)                       -- this shows those who cross-sold within the first year to a different product family
--and a.sen not in (select * from cross_sell)                   -- this shows those who did not cross-sold within the first year to a different product family
--and c.prod_count <= 1                                         -- single land prod
--and c.prod_count > 1                                          -- multiple land prod
--and d.financial_year in ('FY2014','FY2015')                   -- 1-3 years tenure       
--and d.financial_year in ('FY2013','FY2012','FY2011')              -- 4-6 years tenure
--and d.financial_year not in ('FY2016','FY2015','FY2014','FY2013','FY2012','FY2011')        -- 7+ years tenure
group by 1,2
order by 1,2

       