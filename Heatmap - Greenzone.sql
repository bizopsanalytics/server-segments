with land_user_tier as ( 
select  a.sen,
        a.email_domain,
        a.user_limit,
        a.platform,
        a.product_family,
        a.region,
        a.sold_via_partner,
        b.company_size,
        min(a.date) as min_date
from public.sale as a
left join playground_bizops.customer_size as b on a.email_domain = b.email_domain   
where   a.date < '20150331'  
and     a.license_level = 'Full'
and     a.platform = 'Server'
and     a.base_product in ('JIRA','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
group by 1,2,3,4,5,6,7,8
),
first_date as 
( select a.email_domain, a.sen, min(b.date) as min_date
  from land_user_tier as a
  left join public.sale as b on a.email_domain = b.email_domain
  where sale_type = 'New to New'
  and b.base_product in ('JIRA','JIRA Agile', 'JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
  group by 1,2
),
land_prod as
(       select a.email_domain, count(distinct b.base_product) as prod_count
        from first_date as a
        left join public.sale as b on a.email_domain = b.email_domain
        where sale_type = 'New to New'
        and b.base_product in ('JIRA','JIRA Agile', 'JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and license_level in ('Full','Starter')
        and a.min_date = b.date
        group by 1
),
cust_age as
( select a.email_domain, financial_year
        from first_date as a
        left join public.sale as b on a.email_domain = b.email_domain
        where sale_type = 'New to New'
        and b.base_product in ('JIRA','JIRA Agile', 'JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
        and license_level in ('Full','Starter')
        and a.min_date = b.date
        group by 1,2
),
renewed_sens as (
select  a.sen
from    land_user_tier as a
left join public.sale as b on a.sen = b.sen
where   b.sale_type = 'Renewal'
and     b.date > a.min_date
and     b.platform = 'Server'
group by 1
),
addons as (
        select  a.email_domain, 
                a.sen,
                c.base_product,
                count(b.product) as addon_count
                from land_user_tier as a 
                left join public.license as b on a.email_domain = b.tech_email_domain
                left join public.sale as c on a.email_domain = c.email_domain
                where b.platform = 'Server'
                and b.start_date < a.min_date + 365
                and b.base_product = 'Marketplace Addon'
                and a.product_family = b.product_family
                and c.date = a.min_date
        group by 1,2,3
),
no_addons as
(
select sen 
from land_user_tier
where sen not in (
                select  a.sen
                from land_user_tier as a 
                left join public.license as b on a.email_domain = b.tech_email_domain
                where b.platform = 'Server'
                and b.start_date < a.min_date + 365
                and b.base_product = 'Marketplace Addon'
                and a.product_family = b.product_family
                group by 1)
),
coown as (
       select  a.sen
       from land_user_tier as a
       left join (
                select a.email_domain, count(distinct c.base_product) as prod_count
                from land_user_tier as a 
                left join public.sale as c on a.email_domain = c.email_domain
                where cast(c.date as timestamp) < a.min_date + interval '365' DAY
                and c.base_product in('JIRA','JIRA Software','JIRA Service Desk','Confluence','HipChat','Bitbucket','Bamboo','FishEye','Crucible')
                and c.platform = 'Server'
                and c.license_level = 'Full'
        group by 1
        ) as b on a.email_domain = b.email_domain
        where b.prod_count <= 1 -- this controls the coownership
)       
select  a.region,
        a.company_size,
        a.user_limit,
        count(a.sen) as total_sen,
        count(b.sen) as total_renew,
        count(case when sold_via_partner = true then a.sen else null end) as "Expert_SEN",
        count(case when sold_via_partner = true then b.sen else null end) as "Expert_Renew",
        count(case when sold_via_partner = false then a.sen else null end) as "Direct_SEN",
        count(case when sold_via_partner = false then b.sen else null end) as "Direct_Renew"
           
from    land_user_tier as a
left join renewed_sens as b on a.sen = b.sen
left join land_prod as c on a.email_domain = c.email_domain     --linked to product lands
left join cust_age as d on a.email_domain = d.email_domain
where region <> 'Unknown'
--and a.sen in (select sen from addons where addon_count >= 1)    -- this shows only addon group when active
--and a.sen in (select * from no_addons)                        -- this shows only no-addon group when active
--and a.sen in (select * from coown)              -- this shows those who co-owned a primary product within the first year to a different product family
--and c.prod_count <= 1                                         -- single land prod
--and c.prod_count > 1                                          -- multiple land prod
--and d.financial_year in ('FY2015')                   -- 1 years tenure
--and d.financial_year in ('FY2014')                   -- 2 years tenure    
--and d.financial_year in ('FY2013')                   -- 3 years tenure      
--and d.financial_year in ('FY2012','FY2011','FY2010')              -- 4-6 years tenure
--and d.financial_year in ('FY2009','FY2008','FY2007','FY2006','FY2005','FY2004','FY2003','FY2002')        -- 7+ years tenure
group by 1,2,3
order by 1,2,3

       